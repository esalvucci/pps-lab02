package u02lab.code;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.IntStream;

import static org.junit.Assert.*;

public class SequenceGeneratorTest {

    private SequenceGenerator range;
    private static final int START = 5;
    private static final int STOP = 10;
    private static final int INCREMENT_VALUE = 2;

    @Before
    public void setUp() throws Exception {

        this.range = new RangeGenerator.Builder()
                    .setStartRangeElement(START)
                    .setStopRangeElement(STOP)
                    .build();
    }

    @Test
    public void isExpectedNext() {
        this.range.next();
        this.range.next();
        this.range.reset();
        Assert.assertEquals(Optional.of(START + INCREMENT_VALUE), this.range.next());
    }

    @Test
    public void isNextPresent() {
        this.rangeIterator();
        Assert.assertFalse(this.range.next().isPresent());
    }

    @Test
    public void allRemaining() {
        this.range.next();
        List<Integer> list = new LinkedList<>();
        IntStream.range(START + INCREMENT_VALUE, STOP).forEach(list::add);
        Assert.assertEquals(this.range.allRemaining(), list);
    }

    private void rangeIterator() {
        this.range.next();
        for (int i = START; i <= STOP; i++) {
            this.range.next();
        }
    }
}