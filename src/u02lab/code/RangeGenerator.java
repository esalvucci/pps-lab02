package u02lab.code;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * Step 1
 *
 * Using TDD approach (create small test, create code that pass test, refactor to excellence)
 * implement the below class that represents the sequence of numbers from start to stop included.
 * Be sure to test that:
 * - the produced elements (called using next(), go from start to stop included)
 * - calling next after stop leads to a Optional.empty
 * - calling reset after producing some elements brings the object back at the beginning
 * - isOver can actually be called in the middle and should give false, at the end it gives true
 * - can produce the list of remaining elements in one shot
 */
public class RangeGenerator implements SequenceGenerator {

    private int startRangeElement;
    private int currentRangeElement;
    private int stopRangeElement;
    private static final int INCREMENT_VALUE = 2;

    private RangeGenerator(int start, int stop){
        this.startRangeElement = start;
        this.currentRangeElement = this.startRangeElement;
        this.stopRangeElement = stop;
    }

    @Override
    public Optional<Integer> next() {
        if (this.isOver()) {
            return Optional.empty();
        } else {
            this.increment();
            return Optional.of(this.currentRangeElement);
        }
    }

    @Override
    public void reset() {
        this.currentRangeElement = this.startRangeElement;

    }

    @Override
    public boolean isOver() {
        return this.currentRangeElement > this.stopRangeElement;
    }

    @Override
    public List<Integer> allRemaining() {
        List<Integer> list = new LinkedList<>();
        IntStream.range(this.currentRangeElement, this.stopRangeElement)
                .forEach(list::add);
        return list;
    }

    private void increment() {
        this.currentRangeElement = this.currentRangeElement + INCREMENT_VALUE;
    }

    public static class Builder {

        private int startRangeElement;
        private int stopRangeElement;
        private static final int EMPTY = 0;

        Builder() {

        }

        Builder setStartRangeElement(int startRangeElement) {
            this.startRangeElement = startRangeElement;
            return this;
        }

        Builder setStopRangeElement(int stopRangeElement) {
            this.stopRangeElement = stopRangeElement;
            return this;
        }

        SequenceGenerator build() throws IllegalStateException{
            if (this.startRangeElement == EMPTY || this.stopRangeElement == EMPTY) {
                throw new IllegalArgumentException();
            }
            return new RangeGenerator(this.startRangeElement, this.stopRangeElement);
        }
    }
}
